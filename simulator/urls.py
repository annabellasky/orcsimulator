from django.conf.urls import url

from . import views

app_name = 'simulator'
urlpatterns = [
    url('', views.home),
]
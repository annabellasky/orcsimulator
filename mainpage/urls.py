from django.conf.urls import url

from mainpage import views

app_name = 'mainpage'
urlpatterns = [
    url('', views.home),
]
